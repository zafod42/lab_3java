import java.util.ArrayList;
import java.util.Collection;

public class Tests
{

    public static void test1()
    {
        System.out.println("\nTest 1");

        // segregate(Млекопитающие, Ежовые, Кошачьи, Хищные)
        Collection<Mammals> mammals = new ArrayList<>();
        mammals.add(new Hedgehog());
        mammals.add(new Hedgehog());
        mammals.add(new Hedgehog());

        mammals.add(new Manul());
        mammals.add(new Manul());
        mammals.add(new Manul());
        mammals.add(new Manul());

        mammals.add(new Lynx());
        mammals.add(new Lynx());

        Collection<Urchins> urchins = new ArrayList<>();
        Collection<Felines> felines = new ArrayList<>();
        Collection<Predators> predators = new ArrayList<>();


        Segregation.segregate(mammals, urchins, felines, predators);


        System.out.println("Ежовые:");
        for (Urchins urchin : urchins)
        {
            urchin.display();
        }
        System.out.println("Кошачьи:");
        for (Felines feline : felines)
        {
            feline.display();
        }
        System.out.println("Хищные:");
        for (Predators predator : predators)
        {
            predator.display();
        }
    }
    public static void test2()
    {

        System.out.println("\nTest 2");
        // segregate(Хищные, Хордовые, Манулы, Кошачьи)
        Collection<Predators> predators = new ArrayList<>();

        predators.add(new Manul());
        predators.add(new Manul());
        predators.add(new Manul());
        predators.add(new Manul());
        predators.add(new Manul());

        predators.add(new Lynx());
        predators.add(new Lynx());
        predators.add(new Lynx());

        Collection<Chordates> chordates = new ArrayList<>();
        Collection<Manul> manuls = new ArrayList<>();
        Collection<Felines> felines = new ArrayList<>();

        Segregation.segregate(predators, chordates, manuls, felines);

        System.out.println("Хордовые:");
        for (Chordates chordate : chordates)
        {
            chordate.display();
        }
        System.out.println("Манулы:");
        for (Manul manul : manuls)
        {
            manul.display();
        }
        System.out.println("Кошачьи:");
        for (Felines feline : felines)
        {
            feline.display();
        }

    }
    public static void test3()
    {

        System.out.println("\nTest 3");
        // segregate(Ежовые, Насекомоядные, Хищные, Хищные)
        Collection<Urchins> urchins = new ArrayList<>();

        urchins.add(new Hedgehog());
        urchins.add(new Hedgehog());
        urchins.add(new Hedgehog());
        urchins.add(new Hedgehog());
        urchins.add(new Hedgehog());

        Collection<Insectivores> insectivores = new ArrayList<>();
        Collection<Predators> predators1 = new ArrayList<>();
        Collection<Predators> predators2 = new ArrayList<>();

        Segregation.segregate(urchins, insectivores, predators1, predators2);

        System.out.println("Ежовые:");
        for (Urchins urchin : urchins)
        {
            urchin.display();
        }
        System.out.println("Хищные_1:");
        for (Predators predator : predators1)
        {
            predator.display();
        }
        System.out.println("Хищные_2:");
        for (Predators predator : predators2)
        {
            predator.display();
        }

    }
}
