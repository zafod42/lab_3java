import java.util.Collection;

public class Segregation
{
    public static void segregate(Collection<? extends Chordates> srcCollection,
                                 Collection<? super Hedgehog> collection1,
                                 Collection<? super Manul> collection2,
                                 Collection<? super Lynx> collection3)
    {
        for (Chordates animal : srcCollection)
        {
            if (animal instanceof Hedgehog hedgehog)
            {
                collection1.add(hedgehog);
            }
            else if (animal instanceof Manul manul)
            {
                collection2.add(manul);
            }
            else if (animal instanceof Lynx lynx) {
                collection3.add(lynx);
            }
        }
    }
}
